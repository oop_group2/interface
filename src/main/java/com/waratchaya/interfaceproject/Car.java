/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.interfaceproject;

/**
 *
 * @author Melon
 */
public class Car extends Vahicle implements Runable{

    private String nickname;
    
    public Car(String nickname) {
        super("engine");
        this.nickname = nickname;
    }

    @Override
    public void startEngine() {
        System.out.println("Car: " + nickname + " startEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: " + nickname + " stopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car: " + nickname + " raiseSpeede");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car: " + nickname + " applyBreak");
    }

    @Override
    public void run() {
        System.out.println("Car: " + nickname + " run");
    }
    
    
}

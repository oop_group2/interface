/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.interfaceproject;

/**
 *
 * @author Melon
 */
public class Plane extends Vahicle implements Flyable, Runable {

    private String nickname;

    public Plane(String nickname) {
        super("engine");
        this.nickname = nickname;
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: " + nickname + " startEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: " + nickname + " stopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: " + nickname + " raiseSpeede");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: " + nickname + " applyBreak");
    }

    @Override
    public void fly() {
        System.out.println("Plane: " + nickname + " fly");
    }

    @Override
    public void run() {
        System.out.println("Plane: " + nickname + " run");
    }

}

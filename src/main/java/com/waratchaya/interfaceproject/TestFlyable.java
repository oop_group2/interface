/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.interfaceproject;

/**
 *
 * @author Melon
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("GluayHom");
        Plane plane = new Plane("Engine numberI");
        bat.fly(); //Animal,Poultry,Flyable
        plane.fly(); //Vahicle,Flyable
        Dog dog = new Dog("Moji");
        Car car = new Car("Cake");
        
        Flyable[] flyables = {bat, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }

        Runable[] runables = {dog, plane,car};
        for (Runable r : runables) {
            if(r instanceof Car) {
                Car c = (Car) r;
                c.startEngine();
            }
            r.run();
        }
    }
}
